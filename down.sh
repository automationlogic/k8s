#!/bin/bash
export KUBERNETES_PROVIDER=aws
export AWS_DEFAULT_PROFILE=al

export KUBE_AWS_ZONE=eu-west-1b
export NUM_NODES=3
export NUM_MINIONS=$NUM_NODES
export NODE_SIZE=t2.small
export MINION_SIZE=$NODE_SIZE
export AWS_S3_REGION=eu-west-1
export INSTANCE_PREFIX=k8s
./kubernetes/cluster/kube-down.sh
